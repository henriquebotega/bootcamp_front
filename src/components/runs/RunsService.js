export default class RunsService {
    constructor(domain) {
        this.domain = domain || "http://localhost:8080"

        this.salvar = this.salvar.bind(this)
        this.excluir = this.excluir.bind(this)
        this.getAll = this.getAll.bind(this)
    }

    getToken() {
        return localStorage.getItem('id_token')
    }

    excluir(id) {
        return this.fetch(this.domain + "/runs/" + id, {
            method: 'DELETE'
        }).then((res) => {
            return Promise.resolve(res)
        })
    }

    salvar(params) {
        if (params.id) {

            return this.fetch(this.domain + "/runs/" + params.id, {
                method: 'PATCH',
                body: JSON.stringify({
                    distance: params.distance,
                    duration: params.duration,
                    friendly_name: params.friendly_name
                })
            }).then((res) => {
                return Promise.resolve(res)
            })

        } else {

            return this.fetch(this.domain + "/runs", {
                method: 'POST',
                body: JSON.stringify({
                    distance: params.distance,
                    duration: params.duration,
                    friendly_name: params.friendly_name
                })
            }).then((res) => {
                return Promise.resolve(res)
            })

        }
    }

    formatarCreated(vlr) {
        if (vlr) {
            var dt_hr = vlr.split(' ');
            var dt = dt_hr[0].split('-');

            return dt[2] + '/' + dt[1] + '/' + dt[0] + ' ' + dt_hr[1];
        }

        return '';
    }

    formatarDuration(vlr) {
        return (vlr['duration'] / 60).toFixed(2);
    }

    formatarDistance(vlr) {
        if (vlr['unit'] === 'metric') {
            return (vlr['distance'] / 100).toFixed(2) + ' (m)';
        } else {
            return vlr['distance'] + ' (i)';
        }
    }

    getAll() {
        return this.fetch(this.domain + "/runs", {
            method: 'GET'
        }).then((res) => {

            var colData = res['data'].map((obj) => {
                obj['distance_format'] = this.formatarDistance(obj)
                obj['duration_format'] = this.formatarDuration(obj)
                obj['created_format'] = this.formatarCreated(obj['created'])
                return obj;
            })

            return Promise.resolve(colData)
        })
    }

    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + this.getToken()
        }

        return fetch(url, { headers, ...options }).then(this._checkStatus).then((response) => response.json())
    }

    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }
}