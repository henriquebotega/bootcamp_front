import React from 'react'
import './login.css'
import AuthService from './AuthService';
import swal from 'sweetalert'

class Login extends React.Component {

    constructor() {
        super()
        this.handleChange = this.handleChange.bind(this)
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.Auth = new AuthService();
    }

    componentWillMount() {
        if (this.Auth.loggedIn()) {
            this.props.history.replace('/');
        }
    }

    handleFormSubmit(e) {

        e.preventDefault();

        if (this.state && this.state.username && this.state.password) {
            this.Auth.login(this.state.username, this.state.password)
                .then(res => {
                    if (!res['error']) {
                        window.location.href = '/'
                    } else {
                        swal({
                            title: "Aviso",
                            text: "As informações estão inválidas",
                            icon: "warning"
                        });
                    }
                })
                .catch(err => {
                    swal({
                        title: "Erro",
                        text: err,
                        icon: "danger"
                    });
                })
        } else {
            swal({
                title: "Aviso",
                text: "Preencha corretamente os campos solicitados",
                icon: "warning"
            });
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className='center'>
                <div className='card'>
                    <h1>- Bootcamp -</h1>

                    <form onSubmit={this.handleFormSubmit}>

                        <div className="input-field col s6">
                            <input id="username" name="username" type="text" className="validate" onChange={this.handleChange} />
                            <label htmlFor="username">Informe seu login</label>
                        </div>

                        <div className="input-field col s6">
                            <input id="password" name="password" type="password" className="validate" onChange={this.handleChange} />
                            <label htmlFor="password">Informe sua senha</label>
                        </div>

                        <input className="waves-effect waves-light btn" value="VALIDAR" type="submit" />
                    </form>
                </div>
            </div>
        )
    }
}

export default Login