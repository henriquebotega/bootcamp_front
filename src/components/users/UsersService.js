export default class UsersService {
    constructor(domain) {
        this.domain = domain || "http://localhost:8080"

        this.salvar = this.salvar.bind(this)
        this.excluir = this.excluir.bind(this)
        this.getAll = this.getAll.bind(this)
    }

    getToken() {
        return localStorage.getItem('id_token')
    }

    excluir(id) {
        return this.fetch(this.domain + "/users/" + id, {
            method: 'DELETE'
        }).then((res) => {
            return Promise.resolve(res)
        }).catch((err) => {
            return Promise.reject(err)
        })
    }

    salvar(params) {
        if(params.id) {

            return this.fetch(this.domain + "/users/" + params.id, {
                method: 'PATCH',
                body: JSON.stringify({
                    name: params.username,
                    email: params.email,
                    role: params.role,
                    unit: params.unit,
                    timezone: "America/Sao_Paulo"
                })
            }).then((res) => {
                return Promise.resolve(res)
            })

        } else {

            return this.fetch(this.domain + "/users", {
                method: 'POST',
                body: JSON.stringify({
                    name: params.username,
                    email: params.email,
                    passwd: params.password,
                    role: params.role,
                    unit: params.unit,
                    timezone: "America/Sao_Paulo"
                })
            }).then((res) => {
                return Promise.resolve(res)
            })

        }
    }

    getAll() {
        return this.fetch(this.domain + "/users", {
            method: 'GET'
        }).then((res) => {
            return Promise.resolve(res)
        })
    }

    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + this.getToken()
        }

        return fetch(url, { headers, ...options }).then(this._checkStatus).then((response) => response.json())
    }

    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }
}