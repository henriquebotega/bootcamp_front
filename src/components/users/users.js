import React from 'react'
import './users.css'
import UsersService from './UsersService';
import swal from 'sweetalert'

import M from "materialize-css/dist/js/materialize.min.js";
import $ from 'jquery';

class Users extends React.Component {

    constructor() {
        super()

        this.state = {
            id: null,
            username: '',
            email: '',
            password: '',
            role: '',
            unit: '',
            colUsers: []
        }

        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.handleDelUser = this.handleDelUser.bind(this)
        this.handleEditUser = this.handleEditUser.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.getAll = this.getAll.bind(this)

        this.usersService = new UsersService()
    }

    componentWillMount() {
        this.getAll();
    }

    getAll() {
        this.usersService.getAll().then((res) => {
            this.setState({ colUsers: res })
        }).catch(err => {
            swal({ title: "Erro", text: err, icon: "danger" });
        })
    }

    handleDelUser(obj) {
        this.usersService.excluir(obj.id).then(res => {
            swal({ title: "Aviso", text: 'Registro removido com sucesso', icon: "info" });
            this.getAll();
        }).catch((err) => {
            swal({ title: "Aviso", text: 'Vocẽ não tem permissão para esta ação', icon: "warning" });
        })
    }

    handleEditUser(obj) {
        this.setState({
            id: obj.id,
            username: obj.name,
            email: obj.email,
            password: obj.passwd,
            role: obj.role,
            unit: obj.unit
        })

        $(document).ready(function () {
            M.updateTextFields();
        });
    }

    handleFormSubmit(e) {

        e.preventDefault();

        if (this.state && this.state.username && this.state.email && this.state.role && this.state.unit && this.state.password) {

            this.usersService.salvar(this.state)
                .then(res => {

                    swal({ title: "Aviso", text: "O registro foi salvo com sucesso", icon: "success" });

                    this.setState({
                        id: null,
                        username: '',
                        email: '',
                        password: '',
                        role: '',
                        unit: ''
                    })

                    this.getAll();

                })
                .catch(err => {
                    swal({ title: "Erro", text: err, icon: "danger" });
                })
        } else {
            swal({ title: "Aviso", text: "Preencha corretamente os campos solicitados", icon: "warning" });
        }
    }

    handleCancelar() {
        this.setState({
            id: null,
            username: '',
            email: '',
            password: '',
            role: '',
            unit: ''
        })
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleUserUnit(obj) {
        this.setState({ unit: obj })
    }

    handleUserRole(obj) {
        this.setState({ role: obj })
    }

    render() {
        return (
            <div className='cardUser'>
                <h5>Gerenciamento de Usuários</h5>

                <form onSubmit={this.handleFormSubmit}>

                    <fieldset>
                        <legend>Informações do registro</legend>

                        <div className="input-field col s1">
                            <a onClick={() => { this.handleUserUnit('metric') }} className={"btn-floating btn-small waves-effect waves-light " + (this.state.unit === 'metric' ? 'gray' : 'grey')} title='metric'><i className="material-icons">brightness_high</i></a>
                            <a onClick={() => { this.handleUserUnit('imperial') }} className={"btn-floating btn-small waves-effect waves-light " + (this.state.unit === 'imperial' ? 'gray' : 'grey')} title='imperial'><i className="material-icons">brightness_low</i></a>
                        </div>

                        {this.state.id === null &&
                            <div className="input-field col s1">
                                <a onClick={() => { this.handleUserRole('admin') }} className={"btn-floating btn-small waves-effect waves-light " + (this.state.role === 'admin' ? 'gray' : 'grey')} title='admin'><i className="material-icons">account_circle</i></a>
                                <a onClick={() => { this.handleUserRole('user') }} className={"btn-floating btn-small waves-effect waves-light " + (this.state.role === 'user' ? 'gray' : 'grey')} title='usuario'><i className="material-icons">perm_identity</i></a>
                            </div>
                        }

                        <div className="input-field col s3">
                            <input id="username" name="username" type="text" className="validate" onChange={this.handleChange} value={this.state.username} />
                            <label htmlFor="username">Nome</label>
                        </div>

                        {this.state.id === null &&
                            <div className="input-field col s3">
                                <input id="email" name="email" type="text" className="validate" onChange={this.handleChange} value={this.state.email} />
                                <label htmlFor="email">E-mail</label>
                            </div>
                        }

                        <div className="input-field col s3">
                            <input id="password" name="password" type="password" className="validate" onChange={this.handleChange} value={this.state.password} />
                            <label htmlFor="password">Senha</label>
                        </div>

                        <div className="input-field col s1">
                            <button className="btn-floating btn-small waves-effect waves-light btn" type="submit">
                                <i className="material-icons">save</i>
                            </button>

                            <button className="btn-floating btn-small waves-effect waves-light btn gray" type='button' onClick={() => { this.handleCancelar() }}>
                                <i className="material-icons">cancel</i>
                            </button>
                        </div>
                    </fieldset>
                </form>

                <table>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Role</th>
                            <th>Unit</th>
                            <th>Timezone</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        {
                            this.state.colUsers.map((obj, i) => {
                                return (
                                    <tr key={i}>
                                        <td>{obj.name}</td>
                                        <td>{obj.email}</td>
                                        <td>
                                            {obj.role === 'admin' &&
                                                <i className="material-icons" title='admin'>account_circle</i>
                                            }

                                            {obj.role === 'user' &&
                                                <i className="material-icons" title='usuario'>perm_identity</i>
                                            }
                                        </td>
                                        <td>
                                            {obj.unit === 'metric' &&
                                                <i className="material-icons" title='metric'>brightness_high</i>
                                            }

                                            {obj.unit === 'imperial' &&
                                                <i className="material-icons" title='imperial'>brightness_low</i>
                                            }
                                        </td>
                                        <td>{obj.timezone}</td>
                                        <td>
                                            <a onClick={() => { this.handleEditUser(obj) }} className="btn-floating btn-small waves-effect waves-light orange"><i className="material-icons">edit</i></a>
                                            <a onClick={() => { this.handleDelUser(obj) }} className="btn-floating btn-small waves-effect waves-light red"><i className="material-icons">delete</i></a>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Users