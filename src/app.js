import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom'
import Principal from './principal';

import AuthService from './components/AuthService';
import withAuth from './components/withAuth';
const Auth = new AuthService();

import 'materialize-css/dist/css/materialize.min.css';
import M from "materialize-css/dist/js/materialize.min.js";
import Users from './components/users/users';
import Runs from './components/runs/runs';

class App extends Component {

  constructor(props) {
    super(props)
    this.handleLogout = this.handleLogout.bind(this);
    this.Auth = new AuthService();
  }

  handleFecharMenu() {
    M.Sidenav.getInstance(document.querySelector(".sidenav")).close();
  }

  handleLogout() {
    Auth.logout()
    
    var instance = M.Sidenav.getInstance(document.querySelector(".sidenav"));
    instance.close();
    instance.destroy();

    window.location.href = '/login'
  }

  componentDidMount() {
    M.Dropdown.init(document.querySelector('.dropdown-trigger'));

    M.Sidenav.init(document.querySelector(".sidenav"), {
      edge: "left",
      inDuration: 250
    });
  }

  render() {
    return (
      <div>
        <ul id="slide-out" className="sidenav">
          <li>
            <div className="user-view backgroundLogotipo">
              <a href="#name"><span className="white-text name">{this.props.user.name}</span></a>
              <a href="#email"><span className="white-text email">{this.props.user.email}</span></a>
            </div>
          </li>

          <div>
            <li><Link to='/' onClick={this.handleFecharMenu}><i className="material-icons">home</i>Principal</Link></li>
            <li><div className="divider"></div></li>
            <li><Link to='/users' onClick={this.handleFecharMenu}><i className="material-icons">account_circle</i>Usuários</Link></li>
            <li><Link to='/runs' onClick={this.handleFecharMenu}><i className="material-icons">directions_run</i>Corridas</Link></li>
            <li><div className="divider"></div></li>
            <li><Link to='#' onClick={this.handleLogout}><i className="material-icons">power_settings_new</i>Logout</Link></li>
          </div>
        </ul>

        <div className="row">
          <div className="menu sidenav-trigger" data-target="slide-out" >
            <i className="material-icons">menu</i>
          </div>
          <div className="conteudo">

            <div className="navbar-fixed">
              <nav>
                <div className="nav-wrapper">
                  <a href="#!" className="brand-logo center">Bootcamp Front</a>
                </div>
              </nav>
            </div>

            <div className="content-app">
              <Switch>
                <Route exact path='/' component={Principal} />
                <Route exact path='/users' component={Users} />
                <Route exact path='/runs' component={Runs} />
                <Route render={() => <h1>Página não encontrada</h1>} />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withAuth(App);