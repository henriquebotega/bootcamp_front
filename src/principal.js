import React, { Component } from 'react';
import UsersService from './components/users/UsersService';
import RunsService from './components/runs/RunsService';
import { Link } from 'react-router-dom'

class Principal extends Component {

    constructor() {
        super()

        this.state = {
            colUsers: [],
            colRuns: []
        }

        this.runsService = new RunsService()
        this.usersService = new UsersService()
    }

    componentWillMount() {
        this.getAllUsers();
        this.getAllRuns();
    }

    getAllUsers() {
        this.usersService.getAll().then((res) => {
            this.setState({ colUsers: res })
        }).catch(err => {
            console.log(err)
        })
    }

    getAllRuns() {
        this.runsService.getAll().then((res) => {
            this.setState({ colRuns: res })
        }).catch(err => {
            console.log(err)
        })
    }

    render() {
        return (
            <div>

                <div className="row">
                    <div className="col m4">
                        <div className="card blue-grey darken-1">
                            <div className="card-content white-text">
                                <span className="card-title">Usuários</span>
                                <p>Total: {this.state.colUsers.length} registro(s)</p>
                            </div>
                            <div className="card-action">
                                <Link to="/users">Ver mais</Link>
                            </div>
                        </div>
                    </div>

                    <div className="col m4">
                        <div className="card blue-grey darken-1">
                            <div className="card-content white-text">
                                <span className="card-title">Corridas</span>
                                <p>Total: {this.state.colRuns.length} registro(s)</p>
                            </div>
                            <div className="card-action">
                                <Link to='/runs'>Ver mais</Link>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default Principal