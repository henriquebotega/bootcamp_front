import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './estilo.css';

import App from './app';
import Login from './components/login';

ReactDOM.render(
    <Router>
        <div>
            <Route path='/login' component={Login} />
            <Route path='/' component={App} />
        </div>
    </Router>
, document.getElementById('root'));